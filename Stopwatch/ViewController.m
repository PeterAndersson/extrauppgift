//
//  ViewController.m
//  Stopwatch
//
//  Created by IT-Högskolan on 2015-04-19.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *display;
@property (weak, nonatomic) IBOutlet UIButton *button;

- (IBAction)buttonClicked:(id)sender;
- (IBAction)resetButtonClicked:(id)sender;

@end

@implementation ViewController {
    
    bool start;
    NSTimeInterval time;
    int minutes, seconds, stopMinutes, stopSeconds;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.display.text = @"00:00";
    
    start = false;
}

-(void)update {
    
    if(start == false){
        return;
    }
    
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval elapsedTime = currentTime - time;
    
    minutes = (int)(elapsedTime / 60.0);
    seconds = (int)(elapsedTime = elapsedTime - (minutes * 60));
    
    seconds = seconds + stopSeconds;
    if (seconds > 59) {
        seconds = seconds - 60;
        minutes = minutes + 1;
    }
    minutes = minutes + stopMinutes;
    
    self.display.text = [NSString stringWithFormat:@"%02u:%02u", minutes, seconds];
    [self performSelector:@selector(update) withObject:self afterDelay:0.1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClicked:(id)sender {
    
    if(!start){
        start = true;
        time = [NSDate timeIntervalSinceReferenceDate];
        [sender setTitle:@"PAUSE" forState:UIControlStateNormal];
        [self update];
    }else{
        stopMinutes = minutes;
        stopSeconds = seconds;
        start = false;
        [sender setTitle:@"START" forState:UIControlStateNormal];
    }
    
}

- (IBAction)resetButtonClicked:(id)sender {
    if (start) {
        start = false;
        [self.button setTitle:@"START" forState:UIControlStateNormal];
    }
    stopMinutes = 0;
    stopSeconds = 0;
    self.display.text = @"00:00";
    
}






@end
